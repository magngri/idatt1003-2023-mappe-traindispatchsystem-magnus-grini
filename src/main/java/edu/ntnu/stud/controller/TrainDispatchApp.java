package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.TrainDepartureRegister;
import edu.ntnu.stud.model.TrainDepartureService;
import edu.ntnu.stud.model.TrainDepartureServiceInterface;
import edu.ntnu.stud.view.UserInterface;

/**
 * The {@code TrainDispatchApp} class serves as the entry point for the Train Dispatch System.
 * Initializes the core components of the system including the register,
 * service, and user interface.
 * This class sets up the necessary dependencies and begins the user interaction through the
 * user interface.
 */
public class TrainDispatchApp {

  /**
   * The main method that initializes the system and starts the user interface.
   * It creates instances of {@link TrainDepartureRegister}, {@link TrainDepartureService},
   * and {@link UserInterface},
   * and then invokes the {@code run} method of the {@link UserInterface}
   * to start the application.
   *
   * @param args Command line arguments (not used in this application).
   */
  public static void main(String[] args) {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDepartureServiceInterface service = new TrainDepartureService(register);
    UserInterface ui = new UserInterface(service);
    ui.run();
  }
}