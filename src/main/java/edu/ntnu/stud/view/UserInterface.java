package edu.ntnu.stud.view;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainDepartureServiceInterface;
import java.util.List;
import java.util.Scanner;


/**
 * The {@code UserInterface} class provides a console-based user interface for interacting
 * with the Train Departure System.
 */
public class UserInterface {
  private final Scanner scanner;
  private final TrainDepartureServiceInterface service;

  /**
   * Constructs a new UserInterface with the specified TrainDepartureServiceInterface.
   *
   * @param service The service interface to interact with train departure data.
   */
  public UserInterface(TrainDepartureServiceInterface service) {
    this.service = service;
    scanner = new Scanner(System.in);
  }

  /**
   * Starts the main loop of the user interface, displaying options and handling user input.
   */
  public void run() {
    boolean running = true;
    while (running) {
      displayMainMenu();
      String choice = scanner.nextLine();
      switch (choice) {
        case "1" -> handleAddDeparture();
        case "2" -> handleFindDepartureByTrainNumber();
        case "3" -> handleFindDeparturesByDestination();
        case "4" -> handleDisplayAllDepartures();
        case "5" -> handleRemovePastDepartures();
        case "6" -> handleEditDeparture();
        case "0" -> running = false;
        default -> System.out.println("Invalid option. Please try again.");
      }
    }
  }

  /**
   * Displays the main menu options to the console.
   */
  private void displayMainMenu() {
    System.out.println("\nTrain Dispatch System - Main Menu");
    System.out.println("1. Add New Train Departure");
    System.out.println("2. Find Departure by Train Number");
    System.out.println("3. Find Departures by Destination");
    System.out.println("4. Display All Departures");
    System.out.println("5. Remove Past Departures");
    System.out.println("6. Edit Train Departure");
    System.out.println("0. Exit");
    System.out.print("Enter your choice: ");
  }

  /**
   * Handles the process of adding a new train departure.
   */
  private void handleAddDeparture() {
    System.out.print("Enter departure time (HH:MM): ");
    String departureTimeStr = scanner.nextLine();
    if (departureTimeStr.isEmpty() || !departureTimeStr.matches("\\d{2}:\\d{2}")) {
      System.out.println("Invalid or empty departure time. Please use HH:MM format.");
      return;
    }

    System.out.print("Enter line: ");
    String line = scanner.nextLine();
    if (line.isEmpty()) {
      System.out.println("Line cannot be empty.");
      return;
    }

    System.out.print("Enter train number: ");
    String trainNumber = scanner.nextLine();
    if (trainNumber.isEmpty() || !trainNumber.matches("\\d+")) {
      System.out.println("Invalid or empty train number. Please enter a numeric value.");
      return;
    }

    System.out.print("Enter destination: ");
    String destination = scanner.nextLine();
    if (destination.isEmpty()) {
      System.out.println("Destination cannot be empty.");
      return;
    }

    System.out.print("Enter track number: ");
    String trackStr = scanner.nextLine();
    if (trackStr.isEmpty() || !trackStr.matches("-1|\\d+")) {
      System.out.println(
          "Invalid or empty track number. Please enter a numeric value or -1 if not assigned."
      );
      return;
    }

    System.out.print("Enter delay (HH:MM): ");
    String delayStr = scanner.nextLine();
    if (delayStr.isEmpty() || !delayStr.matches("\\d{2}:\\d{2}")) {
      System.out.println("Invalid or empty delay format. Please use HH:MM format.");
      return;
    }

    boolean added = service.addDeparture(
        departureTimeStr, line, trainNumber, destination, trackStr, delayStr
    );
    if (added) {
      System.out.println("Train departure added successfully.");
    } else {
      System.out.println("Failed to add train departure. Please check the input.");
    }
  }

  /**
   * Handles finding a train departure by its train number.
   */
  private void handleFindDepartureByTrainNumber() {
    System.out.print("Enter train number: ");
    String trainNumber = scanner.nextLine();
    if (trainNumber.isEmpty()) {
      System.out.println("Train number cannot be empty.");
      return;
    }

    TrainDeparture departure = service.findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      System.out.println("Found departure: " + departure);
    } else {
      System.out.println("No departure found with train number " + trainNumber);
    }
  }

  /**
   * Handles finding train departures by destination.
   */
  private void handleFindDeparturesByDestination() {
    System.out.print("Enter destination: ");
    String destination = scanner.nextLine();
    List<TrainDeparture> departures = service.findDeparturesByDestination(destination);
    if (!departures.isEmpty()) {
      departures.forEach(System.out::println);
    } else {
      System.out.println("No departures found for destination " + destination);
    }
  }

  /**
   * Handles displaying all train departures.
   */
  private void handleDisplayAllDepartures() {
    List<TrainDeparture> departures = service.getAllDeparturesSorted();
    if (!departures.isEmpty()) {
      departures.forEach(System.out::println);
    } else {
      System.out.println("No departures to display.");
    }
  }

  /**
   * Handles removing past train departures based on the current time.
   */
  private void handleRemovePastDepartures() {
    System.out.print("Enter current time (HH:MM): ");
    String currentTimeStr = scanner.nextLine();

    if (!currentTimeStr.matches("\\d{2}:\\d{2}")) {
      System.out.println("Invalid time format. Please use the HH:MM format.");
      return;
    }

    service.removePastDepartures(currentTimeStr);
  }

  /**
   * Handles the process of removing a train departure.
   * Uses the train number provided from the edit menu.
   *
   * @param trainNumber The train number of the departure to remove.
   * @return true if the train departure is successfully removed, false otherwise.
   */
  private boolean handleRemoval(String trainNumber) {
    if (confirmRemoval()) {
      service.removeTrainDeparture(trainNumber);
      System.out.println("Train departure removed successfully.");
      return true;
    }
    return false;
  }

  /**
   * Handles the process of editing an existing train departure.
   */
  private void handleEditDeparture() {
    System.out.print("Enter the train number of the departure to edit: ");
    String trainNumber = scanner.nextLine();
    if (trainNumber.isEmpty()) {
      System.out.println("Train number cannot be empty.");
      return;
    }

    TrainDeparture existingDeparture = service.findDepartureByTrainNumber(trainNumber);
    if (existingDeparture == null) {
      System.out.println("No departure found with train number " + trainNumber);
      return;
    }

    boolean editing = true;
    while (editing) {
      System.out.println("Select attribute to edit:");
      System.out.println("1. Departure Time");
      System.out.println("2. Line");
      System.out.println("3. Destination");
      System.out.println("4. Track Number");
      System.out.println("5. Delay");
      System.out.println("6. Remove Train Departure");
      System.out.println("0. Stop Editing");
      System.out.print("Your choice: ");
      String editChoice = scanner.nextLine();

      switch (editChoice) {
        case "1" -> updateDepartureTime(existingDeparture);
        case "2" -> updateLine(existingDeparture);
        case "3" -> updateDestination(existingDeparture);
        case "4" -> updateTrack(existingDeparture);
        case "5" -> updateDelay(existingDeparture);
        case "6" -> {
          if (handleRemoval(trainNumber)) {
            editing = false;
          }
        }
        case "0" -> editing = false;
        default -> System.out.println("Invalid option. Please try again.");
      }
    }
  }

  /**
   * Asks the user to confirm the removal of a train departure.
   * Re-prompts the user if the input is neither 'yes' nor 'no'.
   *
   * @return true if the user confirms with 'yes', false otherwise.
   */
  private boolean confirmRemoval() {
    while (true) {
      System.out.print("Are you sure you want to remove this departure? (yes/no): ");
      String response = scanner.nextLine().trim().toLowerCase();

      if ("yes".equals(response)) {
        return true;
      } else if ("no".equals(response)) {
        return false;
      } else {
        System.out.println("Invalid input. Please answer 'yes' or 'no'.");
      }
    }
  }

  /**
   * Updates the departure time of a given train departure.
   * Displays the current departure time and prompts the user to enter a new time.
   *
   * @param departure The train departure to be updated.
   */
  private void updateDepartureTime(TrainDeparture departure) {
    System.out.println("Current Departure Time: " + departure.getDepartureTime());
    System.out.print("Enter new departure time (HH:MM): ");
    String newTime = scanner.nextLine();
    if (newTime.isEmpty() || !newTime.matches("\\d{2}:\\d{2}")) {
      System.out.println("Invalid or empty time format. Please use HH:MM format.");
      return;
    }
    service.updateDepartureTime(departure.getTrainNumber(), newTime);
    System.out.println("Departure time updated.");
  }

  /**
   * Updates the line of a given train departure.
   * Displays the current line and prompts the user to enter a new line.
   *
   * @param departure The train departure to be updated.
   */
  private void updateLine(TrainDeparture departure) {
    System.out.println("Current Line: " + departure.getLine());
    System.out.print("Enter new line: ");
    String newLine = scanner.nextLine();
    if (newLine.isEmpty()) {
      System.out.println("Line cannot be empty.");
      return;
    }
    service.updateLine(departure.getTrainNumber(), newLine);
    System.out.println("Line updated.");
  }

  /**
   * Updates the destination of a given train departure.
   * Displays the current destination and prompts the user to enter a new destination.
   *
   * @param departure The train departure to be updated.
   */
  private void updateDestination(TrainDeparture departure) {
    System.out.println("Current Destination: " + departure.getDestination());
    System.out.print("Enter new destination: ");
    String newDestination = scanner.nextLine();
    if (newDestination.isEmpty()) {
      System.out.println("Destination cannot be empty.");
      return;
    }
    service.updateDestination(departure.getTrainNumber(), newDestination);
    System.out.println("Destination updated.");
  }

  /**
   * Updates the track number of a given train departure.
   * Displays the current track number and prompts the user to enter a new track number.
   *
   * @param departure The train departure to be updated.
   */
  private void updateTrack(TrainDeparture departure) {
    System.out.println("Current Track Number: " + departure.getTrack());
    System.out.print("Enter new track number: ");
    String newTrackStr = scanner.nextLine();
    if (newTrackStr.isEmpty() || !newTrackStr.matches("-1|\\d+")) {
      System.out.println(
          "Invalid or empty track number. Please enter a numeric value or -1 if not assigned."
      );
      return;
    }
    service.updateTrack(departure.getTrainNumber(), newTrackStr);
    System.out.println("Track number updated.");
  }

  /**
   * Updates the delay of a given train departure.
   * Displays the current delay and prompts the user to enter a new delay time.
   *
   * @param departure The train departure to be updated.
   */
  private void updateDelay(TrainDeparture departure) {
    System.out.println("Current Delay: " + departure.getDelay());
    System.out.print("Enter new delay (HH:MM): ");
    String newDelay = scanner.nextLine();
    if (newDelay.isEmpty() || !newDelay.matches("\\d{2}:\\d{2}")) {
      System.out.println("Invalid or empty delay format. Please use HH:MM format.");
      return;
    }
    service.updateDelay(departure.getTrainNumber(), newDelay);
    System.out.println("Delay updated.");
  }
}