package edu.ntnu.stud.model;

import java.util.List;

/**
 * The {@code TrainDepartureServiceInterface} defines the contract for
 * services managing train departures.
 */
public interface TrainDepartureServiceInterface {

  /**
   * Adds a new train departure.
   *
   * @param departureTimeStr The departure time as a string.
   * @param line             The line of the train.
   * @param trainNumber      The train number.
   * @param destination      The destination of the train.
   * @param trackStr         The track number as a string.
   * @param delayStr         The delay time as a string.
   * @return true if the departure was added successfully, false otherwise.
   */
  boolean addDeparture(
      String departureTimeStr,
      String line,
      String trainNumber,
      String destination,
      String trackStr,
      String delayStr
  );

  /**
   * Finds a train departure by its train number.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture object if found, null otherwise.
   */
  TrainDeparture findDepartureByTrainNumber(String trainNumber);

  /**
   * Finds all departures going to a specific destination.
   *
   * @param destination The destination to search for.
   * @return A list of TrainDeparture objects for the specified destination.
   */
  List<TrainDeparture> findDeparturesByDestination(String destination);

  /**
   * Retrieves all train departures sorted by departure time.
   *
   * @return A sorted list of all TrainDeparture objects.
   */
  List<TrainDeparture> getAllDeparturesSorted();

  /**
   * Removes departures that have already passed relative to the given current time.
   *
   * @param currentTimeStr The current time as a string.
   */
  void removePastDepartures(String currentTimeStr);

  /**
   * Removes a train departure from the register.
   *
   * @param trainNumber The train number of the departure to be removed.
   */
  void removeTrainDeparture(String trainNumber);

  /**
   * Updates the departure time of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newTimeStr  The new departure time as a string.
   */
  void updateDepartureTime(String trainNumber, String newTimeStr);

  /**
   * Updates the line of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newLine     The new line.
   */
  void updateLine(String trainNumber, String newLine);

  /**
   * Updates the destination of a specific train.
   *
   * @param trainNumber    The number of the train to update.
   * @param newDestination The new destination.
   */
  void updateDestination(String trainNumber, String newDestination);

  /**
   * Updates the track number of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newTrackStr The new track number as a string.
   */
  void updateTrack(String trainNumber, String newTrackStr);

  /**
   * Updates the delay of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newDelayStr The new delay time as a string.
   */
  void updateDelay(String trainNumber, String newDelayStr);
}