package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@code TrainDepartureRegister} class represents a register for managing train departures.
 */
public class TrainDepartureRegister {
  private final List<TrainDeparture> departures;

  /**
   * Constructs a new TrainDepartureRegister.
   */
  public TrainDepartureRegister() {
    this.departures = new ArrayList<>();
  }

  /**
   * Adds a train departure to the register.
   *
   * @param departure The train departure to be added.
   * @return {@code true} if the train departure was successfully added; {@code false} otherwise.
   */
  public boolean addTrainDeparture(TrainDeparture departure) {
    if (departure == null || isTrainNumberExists(departure.getTrainNumber())) {
      return false;
    }
    departures.add(departure);
    return true;
  }

  /**
   * Finds a train departure by its train number.
   *
   * @param trainNumber The train number to search for.
   * @return The train departure with the given number, or {@code null} if not found.
   */
  public TrainDeparture findDepartureByTrainNumber(String trainNumber) {
    return departures.stream()
        .filter(departure -> departure.getTrainNumber().equals(trainNumber))
        .findFirst()
        .orElse(null);
  }

  /**
   * Finds all train departures by a given destination.
   *
   * @param destination The destination to search for.
   * @return A list of train departures heading to the specified destination.
   */
  public List<TrainDeparture> findDeparturesByDestination(String destination) {
    return departures.stream()
        .filter(departure -> departure.getDestination().equalsIgnoreCase(destination))
        .collect(Collectors.toList());
  }

  /**
   * Removes departures from the register that have already departed relative to the given time.
   *
   * @param currentTime The current time used to determine past departures.
   */
  public void removePastDepartures(LocalTime currentTime) {
    departures.removeIf(departure -> {
      LocalTime departureTimeWithDelay = departure.getDepartureTime()
          .plusHours(departure.getDelay().getHour())
          .plusMinutes(departure.getDelay().getMinute());
      return departureTimeWithDelay.isBefore(currentTime);
    });
  }

  /**
   * Removes a train departure from the list of departures.
   * The removal is based on the train number.
   *
   * @param trainNumber The train number of the departure(s) to be removed.
   */
  public void removeTrainDeparture(String trainNumber) {
    departures.removeIf(departure -> departure.getTrainNumber().equals(trainNumber));
  }

  /**
   * Retrieves all train departures, sorted by departure time.
   *
   * @return A sorted list of all train departures.
   */
  public List<TrainDeparture> getAllDeparturesSorted() {
    return departures.stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .collect(Collectors.toList());
  }

  /**
   * Checks if a train number already exists in the register.
   *
   * @param trainNumber The train number to check.
   * @return {@code true} if the train number exists; {@code false} otherwise.
   */
  private boolean isTrainNumberExists(String trainNumber) {
    return departures.stream().anyMatch(
        departure -> departure.getTrainNumber().equals(trainNumber)
    );
  }

  /**
   * Updates the departure time of a specific train departure.
   *
   * @param trainNumber The train number of the departure to update.
   * @param newTime     The new departure time to set.
   */
  public void updateDepartureTime(String trainNumber, LocalTime newTime) {
    TrainDeparture departure = findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      departure.setDepartureTime(newTime);
    }
  }

  /**
   * Updates the line of a specific train departure.
   *
   * @param trainNumber The train number of the departure to update.
   * @param newLine     The new line to set.
   */
  public void updateLine(String trainNumber, String newLine) {
    TrainDeparture departure = findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      departure.setLine(newLine);
    }
  }

  /**
   * Updates the destination of a specific train departure.
   *
   * @param trainNumber    The train number of the departure to update.
   * @param newDestination The new destination to set.
   */
  public void updateDestination(String trainNumber, String newDestination) {
    TrainDeparture departure = findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      departure.setDestination(newDestination);
    }
  }

  /**
   * Updates the track number of a specific train departure.
   *
   * @param trainNumber The train number of the departure to update.
   * @param newTrack    The new track number to set.
   */
  public void updateTrack(String trainNumber, int newTrack) {
    TrainDeparture departure = findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      departure.setTrack(newTrack);
    }
  }

  /**
   * Updates the delay of a specific train departure.
   *
   * @param trainNumber The train number of the departure to update.
   * @param newDelay    The new delay time to set.
   */
  public void updateDelay(String trainNumber, LocalTime newDelay) {
    TrainDeparture departure = findDepartureByTrainNumber(trainNumber);
    if (departure != null) {
      departure.setDelay(newDelay);
    }
  }
}