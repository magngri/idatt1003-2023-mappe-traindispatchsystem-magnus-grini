package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents a single train departure, including details such as
 * departure time, line, train number,
 * destination, track, and delay.
 */
public class TrainDeparture {
  private LocalTime departureTime;
  private String line;
  private String trainNumber;
  private String destination;
  private int track;
  private LocalTime delay;

  /**
   * Constructs a new TrainDeparture instance.
   *
   * @param departureTime The time of departure.
   * @param line          The line on which the train operates.
   * @param trainNumber   The unique number of the train.
   * @param destination   The destination of the train.
   * @param track         The track from which the train departs.
   * @param delay         The delay in the train's departure, if any.
   */
  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int track, LocalTime delay) {
    setDepartureTime(departureTime);
    setLine(line);
    setTrainNumber(trainNumber);
    setDestination(destination);
    setTrack(track);
    setDelay(delay);
  }

  /**
   * gets departure time.
   *
   * @return departureTime
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Sets the departure time of the train.
   *
   * @param departureTime The new departure time to be set.
   * @throws IllegalArgumentException If the departure time is null.
   */
  public void setDepartureTime(LocalTime departureTime) {
    validateDepartureTime(departureTime);
    this.departureTime = departureTime;
  }

  /**
   * gets line.
   *
   * @return line
   */
  public String getLine() {
    return line;
  }

  /**
   * Sets the line of the train.
   *
   * @param line The new line to be set.
   * @throws IllegalArgumentException If the line is null or empty.
   */
  public void setLine(String line) {
    if (line == null || line.trim().isEmpty()) {
      throw new IllegalArgumentException("Line cannot be null or empty");
    }
    this.line = line;
  }

  /**
   * gets train number.
   *
   * @return trainNumber
   */
  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Sets the train number.
   *
   * @param trainNumber The new train number to be set.
   * @throws IllegalArgumentException If the train number is null or empty.
   */
  public void setTrainNumber(String trainNumber) {
    if (trainNumber == null || trainNumber.trim().isEmpty()) {
      throw new IllegalArgumentException("Train number cannot be null or empty");
    }
    this.trainNumber = trainNumber;
  }

  /**
   * gets destination.
   *
   * @return destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Sets the destination of the train.
   *
   * @param destination The new destination to be set.
   * @throws IllegalArgumentException If the destination is null or empty.
   */
  public void setDestination(String destination) {
    if (destination == null || destination.trim().isEmpty()) {
      throw new IllegalArgumentException("Destination cannot be null or empty");
    }
    this.destination = destination;
  }

  /**
   * gets track.
   *
   * @return track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Sets the track number from which the train will depart.
   *
   * @param track The new track number to be set.
   * @throws IllegalArgumentException If the track number is less than -1.
   */
  public void setTrack(int track) {
    if (track < -1) {
      throw new IllegalArgumentException("Track must be -1 or positive");
    }
    this.track = track;
  }

  /**
   * gets delay.
   *
   * @return delay
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets the delay time of the train.
   *
   * @param delay The new delay time to be set.
   * @throws IllegalArgumentException If the delay is null or too long.
   */
  public void setDelay(LocalTime delay) {
    validateDelay(delay);
    this.delay = delay;
  }

  /**
   * Validates the departure time.
   *
   * @param time The departure time to validate.
   * @throws IllegalArgumentException If the time is null.
   */
  private void validateDepartureTime(LocalTime time) {
    if (time == null) {
      throw new IllegalArgumentException("Departure time cannot be null");
    }
  }

  /**
   * Validates the delay.
   *
   * @param delay The delay to validate.
   * @throws IllegalArgumentException If the delay is null or too long.
   */
  private void validateDelay(LocalTime delay) {
    if (delay == null) {
      throw new IllegalArgumentException("Delay cannot be null");
    }
    if (delay.isAfter(LocalTime.of(23, 59))) {
      throw new IllegalArgumentException("Delay is too long");
    }
  }

  /**
   * Returns a string representation of the train departure.
   *
   * @return A string that includes all details of the train departure.
   */
  @Override
  public String toString() {
    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    String formattedDepartureTime = departureTime.format(timeFormatter);
    String formattedDelay = delay.equals(
        LocalTime.of(0, 0)) ? "On time" : "Delayed by " + delay.format(timeFormatter
    );

    return "Departure Details:\n"
        + "  Time: " + formattedDepartureTime + "\n"
        + "  Line: " + line + "\n"
        + "  Train Number: " + trainNumber + "\n"
        + "  Destination: " + destination + "\n"
        + "  Track: " + (track == -1 ? "Not assigned" : track) + "\n"
        + "  Status: " + formattedDelay;
  }
}