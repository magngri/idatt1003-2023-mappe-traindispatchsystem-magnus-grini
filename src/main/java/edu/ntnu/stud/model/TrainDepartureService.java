package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.util.List;

/**
 * The {@code TrainDepartureService} class provides services for managing train departures.
 * It implements the {@link TrainDepartureServiceInterface} and
 * delegates operations to a {@link TrainDepartureRegister}.
 */
public class TrainDepartureService implements TrainDepartureServiceInterface {
  private final TrainDepartureRegister register;

  /**
   * Constructs a new TrainDepartureService with the specified TrainDepartureRegister.
   *
   * @param register The register to be used for storing and retrieving train departures.
   */
  public TrainDepartureService(TrainDepartureRegister register) {
    this.register = register;
  }

  /**
   * Adds a new train departure to the register.
   *
   * @param departureTimeStr The departure time as a string.
   * @param line             The line of the train.
   * @param trainNumber      The train number.
   * @param destination      The destination of the train.
   * @param trackStr         The track number as a string.
   * @param delayStr         The delay time as a string.
   * @return true if the departure was added successfully, false otherwise.
   */
  public boolean addDeparture(
      String departureTimeStr,
      String line,
      String trainNumber,
      String destination,
      String trackStr,
      String delayStr
  ) {
    LocalTime departureTime = LocalTime.parse(departureTimeStr);
    int track = Integer.parseInt(trackStr);
    LocalTime delay = LocalTime.parse(delayStr);

    return register.addTrainDeparture(
        new TrainDeparture(departureTime, line, trainNumber, destination, track, delay)
    );
  }

  /**
   * Finds a train departure by its train number.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture object if found, null otherwise.
   */
  public TrainDeparture findDepartureByTrainNumber(String trainNumber) {
    return register.findDepartureByTrainNumber(trainNumber);
  }

  /**
   * Finds all departures with a specific destination.
   *
   * @param destination The destination to search for.
   * @return A list of TrainDeparture objects going to the specified destination.
   */
  public List<TrainDeparture> findDeparturesByDestination(String destination) {
    return register.findDeparturesByDestination(destination);
  }

  /**
   * Retrieves all train departures sorted by departure time.
   *
   * @return A sorted list of all TrainDeparture objects.
   */
  public List<TrainDeparture> getAllDeparturesSorted() {
    return register.getAllDeparturesSorted();
  }

  /**
   * Removes departures that have already passed relative to the given current time.
   *
   * @param currentTimeStr The current time as a string.
   */
  public void removePastDepartures(String currentTimeStr) {
    LocalTime currentTime = LocalTime.parse(currentTimeStr);
    register.removePastDepartures(currentTime);
  }

  /**
   * Removes a train departure identified by the given train number.
   *
   * @param trainNumber The unique identifier of the train departure to be removed.
   */
  public void removeTrainDeparture(String trainNumber) {
    register.removeTrainDeparture(trainNumber);
  }

  /**
   * Updates the departure time of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newTimeStr  The new departure time as a string.
   */
  public void updateDepartureTime(String trainNumber, String newTimeStr) {
    LocalTime newTime = LocalTime.parse(newTimeStr);
    register.updateDepartureTime(trainNumber, newTime);
  }

  /**
   * Updates the line of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newLine     The new line.
   */
  public void updateLine(String trainNumber, String newLine) {
    register.updateLine(trainNumber, newLine);
  }

  /**
   * Updates the destination of a specific train.
   *
   * @param trainNumber    The number of the train to update.
   * @param newDestination The new destination.
   */
  public void updateDestination(String trainNumber, String newDestination) {
    register.updateDestination(trainNumber, newDestination);
  }

  /**
   * Updates the track number of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newTrackStr The new track number as a string.
   */
  public void updateTrack(String trainNumber, String newTrackStr) {
    int newTrack = Integer.parseInt(newTrackStr);
    register.updateTrack(trainNumber, newTrack);
  }

  /**
   * Updates the delay of a specific train.
   *
   * @param trainNumber The number of the train to update.
   * @param newDelayStr The new delay time as a string.
   */
  public void updateDelay(String trainNumber, String newDelayStr) {
    LocalTime newDelay = LocalTime.parse(newDelayStr);
    register.updateDelay(trainNumber, newDelay);
  }
}
