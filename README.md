# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Magnus Grini"  
STUDENT ID = "593608"

## Project description

A simple train dispatch system for creating, viewing, and modifying train departures.

## Project structure

The project implements MVC architecture. Model, View, and Controller are each their own package. All source files are stored under src/main/java and all test files are stored under src/test/java.

## Link to repository

[GitLab Repository Link](https://gitlab.stud.idi.ntnu.no/magngri/idatt1003-2023-mappe-traindispatchsystem-magnus-grini)

## How to run the project

### Main class

TrainDispatchApp.java

### Main method

The main method is within the main class.

### Running the project

Before running the project for the first time, execute the following line in the terminal:
```bash
mvn clean install
```

To run the project, execute the following command in the terminal:

```bash
mvn exec:java
```

## How to run the tests

To run JUnit tests execute the following command in the terminal:
```bash
mvn clean test
```

To run JavaDoc tests execute the following command in the terminal:

```bash
mvn javadoc:javadoc
```
